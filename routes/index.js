var express = require('express');
var router = express();

router.set('view engine','ejs');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', {page:'index', menuId:'index'});
});

//Add

router.get('/hugues_capet', function (req, res, next) {
  res.render('hugues_capet', { page: 'hugues_capet', menuId: 'hugues_capet' });
});

router.get('/louis_ix', function (req, res, next) {
  res.render('louis_ix', { page: 'louis_ix', menuId: 'louis_ix' });
});

router.get('/philippe_auguste', function (req, res, next) {
  res.render('philippe_auguste', { page: 'philippe_auguste', menuId: 'philippe_auguste' });
});

router.get('/philippe_le_bel', function (req, res, next) {
  res.render('philippe_le_bel', { page: 'philippe_le_bel', menuId: 'philippe_le_bel' });
});

router.get('/formulaire', function (req, res, next) {
  res.render('formulaire', { page: 'formulaire', menuId: 'formulaire' });
});

router.use('/public' ,express.static('public'));
module.exports = router;
